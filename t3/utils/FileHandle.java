package utils;

import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileHandle {

	public FileHandle () {
		
	}
	
	public boolean exists (String filePath) {
		File file = new File(filePath);
		if (!file.exists()) {
			return false;
		}
		return true;
	}
	
	public boolean delete (String filePath) {
		try {
			if (!this.exists(filePath)) {
				return false;
			}
			Path p = Paths.get(filePath);
			Files.deleteIfExists(p);
			return true;
		} catch (IOException e) {
			System.out.println("FileHandle delete(): " + e.getMessage());
			return false;
		}
	}
	
	public boolean open (String filePath) {
		try {
			File file = new File(filePath);
			if (!file.exists()) {
				return false;
			}
			Desktop desktop = Desktop.getDesktop();
			desktop.open(file);
			return true;
		} catch (IOException e) {
			System.out.println("FileHandle open(): " + e.getMessage());
			return false;
		}
	}
	
	public byte[] getBytes (String filePath) {
		try {
			File file = new File(filePath);
	    	FileInputStream fileInputStream;
			fileInputStream = new FileInputStream(file);
			int fileLenght = (int) file.length();
	    	byte[] fileBytes = new byte[fileLenght];
	    	fileInputStream.read(fileBytes);
	    	fileInputStream.close();
	    	return fileBytes;
		} catch (IOException e) {
			System.out.println("FileHandle getBytes(): Unable to get bytes from file '" + filePath + "'. " + e.getMessage());
			return null;
		}
	}
	
	public boolean save (String filePath, byte[] fileContent) {
		try {
			File file = new File(filePath);
			if (file.exists()) {
				return false;
			}
			FileOutputStream in = new FileOutputStream(file);
			in.write(fileContent);
			in.close();
			return true;
		} catch (IOException e) {
			System.out.println("FileHandle save(): " + e.getMessage());
			return false;
		}
	}
	
}
