package utils;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Random;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;

public class Keys {
	
	private FileHandle fileHandle;
	
	public Keys () {
		this.fileHandle = new FileHandle();
	}
	
	public PrivateKey getPrivateKey (String keyPath, String seed) {
        try {
        	if (!this.fileHandle.exists(keyPath)) {
        		return null;
        	}
        	
        	// generate symmetric key from seed
	    	KeyGenerator keyGen = KeyGenerator.getInstance("DES");
	    	SecureRandom seedRandom = SecureRandom.getInstance("SHA1PRNG", "SUN");
	    	seedRandom.setSeed(seed.getBytes());	    	
	    	keyGen.init(56, seedRandom);
	    	Key seedKey = keyGen.generateKey();
	    	
	    	// get key bytes from input file, decrypted with symmetric key generated above 
	    	byte[] fileBytes = this.fileHandle.getBytes(keyPath);
	    	Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
	    	cipher.init(Cipher.DECRYPT_MODE, seedKey);
	    	byte[] keyBytes = cipher.doFinal(fileBytes);
	    	
	    	// get private key from key bytes read above
	    	KeyFactory keyFactory = KeyFactory.getInstance("RSA");
	    	PKCS8EncodedKeySpec keyPKCS8 = new PKCS8EncodedKeySpec(keyBytes);
	    	PrivateKey key = keyFactory.generatePrivate(keyPKCS8);
	    	return key;
        } catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException | 
        		BadPaddingException | InvalidKeySpecException | IllegalBlockSizeException | NoSuchProviderException e) {
        	System.out.println("Keys getPrivateKey(): " + e.getMessage());
        	return null;
        }
	}
	
	public PublicKey getPublicKey (String keyPath) {
		byte[] fileBytes = this.fileHandle.getBytes(keyPath);
    	return this.getPublicKey(fileBytes);
	}
	 
	public PublicKey getPublicKey (byte[] publicKey) {
		try {
	    	KeyFactory keyFactory = KeyFactory.getInstance("RSA");
	    	X509EncodedKeySpec keyX509 = new X509EncodedKeySpec(publicKey);
	    	PublicKey key = keyFactory.generatePublic(keyX509);
	    	return key;
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			System.out.println("Keys getPublicKey(): " + e.getMessage());
			return null;
		}
	}
	
	public byte[] getPublicKeyBytes (PublicKey publicKey) {
		return publicKey.getEncoded();
	}
	
	public boolean validateKeys (PrivateKey privateKey, PublicKey publicKey) {
		try {	
			byte[] b = new byte[512];
			new Random().nextBytes(b);
			
			// sign sentence with private key
			Signature sig = Signature.getInstance("MD5WithRSA");
			sig.initSign(privateKey);
			sig.update(b);
			byte[] sSigned = sig.sign();
	    	
	    	// verify sign with public key
			sig.initVerify(publicKey);
			sig.update(b);
			if (!sig.verify(sSigned)) {
				return false;
			}
	    	return true;
		} catch (InvalidKeyException | NoSuchAlgorithmException | SignatureException e) {
			System.out.println("Keys validateKeys(): " + e.getMessage());
			return false;
		}
	}
	
}
