package utils;

import java.util.Random;

public class RandNoRepeat {
	
	private static Integer MAX_LIMIT = 10;
	private Boolean[] list_chosen;
	
	public RandNoRepeat () {
		this.list_chosen = new Boolean[MAX_LIMIT];
		for (int i = 0; i < MAX_LIMIT; i++) {
			this.list_chosen[i] = false;
		}
	}
	
	public int GetNumber () {
	    Random rand = new Random();
	    int randomNum = rand.nextInt(MAX_LIMIT);
	    for (int i = 0; i < MAX_LIMIT; i++) {
	    	Integer num = (randomNum + i) % 10;
	    	if (this.list_chosen[num].equals(false)) {
	    		this.list_chosen[num] = true;
	    		return num;
	    	}
	    }
	    return -1;
	}
}
