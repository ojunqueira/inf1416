package utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Hash {
	
	public Hash () {
		
	}
	
	public String getHash (String message) {		
		try {
		    MessageDigest messageDigest = MessageDigest.getInstance("MD5");
		    messageDigest.update(message.getBytes());
		    byte[] digest = messageDigest.digest();
		    StringBuffer dig_buffer = new StringBuffer();
		    for(int i = 0; i < digest.length; i++) {
		      String hex = Integer.toHexString(0x0100 + (digest[i] & 0x00FF)).substring(1);
		      dig_buffer.append((hex.length() < 2 ? "0" : "") + hex);
		    }
		    return dig_buffer.toString();
		} catch (NoSuchAlgorithmException e) {
			System.out.println("Hash getHash(): " + e.getMessage());
		}
		return new String();
	}
}
