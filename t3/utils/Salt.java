package utils;

import java.util.Random;

public class Salt {
	
	public Salt () {
		
	}
	
	public String getSalt () {
		String res = new String("");
		for (int i = 0; i < 10; i++) {
			Random rand = new Random();
			res = res + rand.nextInt(10);
		}
		return res;
	}
	
}
