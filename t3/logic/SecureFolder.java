package logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.util.ArrayList;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;

import utils.FileHandle;

public class SecureFolder {

	private Logic logic;
	private FileHandle fileHandle;
	private PrivateKey privateKey;
	private PublicKey publicKey;
	private ArrayList<String> listFilesName;
	private ArrayList<String> listFilesSecretName;
	private ArrayList<String> listFilesStatus;
	private ArrayList<String> listFilesNameOpened;
	private String folderPath;
	
	public SecureFolder (Logic logic) {
		this.logic = logic;
		this.fileHandle = new FileHandle();
		this.listFilesName = new ArrayList<String>();
		this.listFilesSecretName = new ArrayList<String>();
		this.listFilesStatus = new ArrayList<String>();
		this.listFilesNameOpened = new ArrayList<String>();
		this.folderPath = new String();
	}
	
	public ArrayList<String> getFilesName () {
		return this.listFilesName;
	}
	
	public ArrayList<String> getSecretFilesName () {
		return this.listFilesSecretName;
	}
	
	public ArrayList<String> getFilesStatus () {
		return this.listFilesStatus;
	}
	
	public void deleteOpenedFiles () {
		boolean deleteAll = true;
		if (this.listFilesNameOpened.size() > 0) {
			for (String file: this.listFilesNameOpened) {
				if (!this.fileHandle.delete(file)) {
					deleteAll = false;
					this.logic.setLogMessage(9005, file);
				}
			}
			if (deleteAll) {
				this.logic.setLogMessage(9004);
			}
		}
	}
	
	public boolean openFile (String fileName) {
		this.logic.setLogMessage(8003, fileName);
		for (int i = 0; i < this.listFilesName.size(); i++) {
			if (this.listFilesName.get(i).equals(fileName)) {
				if (!this.listFilesStatus.get(i).equals("OK")) {
					return false;
				}
				byte[] decryptedFileBytes = this.decryptFile(fileName);
				if (decryptedFileBytes == null) {
					return false;
				}
				String decryptedFile = this.folderPath + "/" + this.listFilesSecretName.get(i);
				if (!this.fileHandle.save(decryptedFile, decryptedFileBytes)) {
					return false;
				}
				this.fileHandle.open(decryptedFile);
				this.listFilesNameOpened.add(this.folderPath +  "/" + this.listFilesSecretName.get(i));
				return true;
			}
		}
		return false;
	}
	
	public boolean openFolder (String folderPath, PrivateKey privateKey, PublicKey publicKey) {
		this.folderPath = folderPath;
		this.privateKey = privateKey;
		this.publicKey = publicKey;
		this.listFilesName = new ArrayList<String>();
		this.listFilesSecretName = new ArrayList<String>();
		this.listFilesStatus = new ArrayList<String>();
		
		byte[] indexBytes = this.decryptFile("index");
		if (indexBytes == null) {
			return false;
		}
		String indexFile = this.folderPath + "/index.txt";
		if (!this.fileHandle.save(indexFile, indexBytes)) {
			return false;
		}
        try {  
            FileReader f_reader = new FileReader(new File(indexFile));
            BufferedReader b_reader = new BufferedReader(f_reader);
            String line;
            while ((line = b_reader.readLine()) != null) {
          	  if (!line.isEmpty()) {
          		  String[] params = line.split(" ");
          		  if (!(params.length == 2)) {
          			  System.out.println("SecureFolder openFolder(): Error reading 'index'. Only two"
          			  		+ " parameters expected on each line.");
          			  b_reader.close();
          			  return false;
          		  }
          		  this.listFilesName.add(params[1]);
          		  this.listFilesSecretName.add(params[0]);
          		  if (this.decryptFile(params[1]) == null) {
          			  this.listFilesStatus.add("NOT OK");
          		  } else {
          			  this.listFilesStatus.add("OK");
          		  }
          	  }
            }
            b_reader.close();
        } catch (IOException e) {
          	System.out.println("SecureFolder openFolder(): Could not open index file '" + indexFile + "': " + e.getMessage());
          	return false;
      	}
        this.fileHandle.delete(indexFile);
        return true;
	}
	
	private byte[] decryptFile (String fileName) {
		String fileEncoded = this.folderPath + "/" + fileName + ".enc";
		String fileEnvelope = this.folderPath + "/" + fileName + ".env";
		String fileSignature = this.folderPath + "/" + fileName + ".asd";
		if (!this.fileHandle.exists(fileEncoded) ||!this.fileHandle.exists(fileEnvelope) || !this.fileHandle.exists(fileSignature)) {
			this.logic.setLogMessage(8006, fileName);
			return null;
		}
		// open envelope file (.env) and get seed bytes
		byte[] decryptedSeedBytes = this.getFileEnvelopeBytes(fileEnvelope);
		if (decryptedSeedBytes == null) {
			this.logic.setLogMessage(8006, fileName);
			return null;
		}
		// open encrypted file (.enc) with symmetric key generated by seed bytes
		byte[] decryptedFileBytes = this.getFileEncryptedBytes(fileEncoded, decryptedSeedBytes);
		if (decryptedFileBytes == null) {
			this.logic.setLogMessage(8006, fileName);
			return null;
		}
	    // open signature file with public key
		byte[] decryptedFileHash = this.getFileSignatureBytes(fileSignature);
		if (decryptedFileHash == null) {
			this.logic.setLogMessage(8006, fileName);
			return null;
		}
		this.logic.setLogMessage(8004, fileName);
		if(!this.validate(decryptedFileBytes, decryptedFileHash)) {
			this.logic.setLogMessage(8007, fileName);
			return null;
		}
		this.logic.setLogMessage(8005, fileName);
		return decryptedFileBytes;
	}
	
	private byte[] getFileEncryptedBytes (String filePath, byte[] seed) {
		byte[] fileBytes = this.fileHandle.getBytes(filePath);
		if (fileBytes == null) {
			return null;
		}
		try {
	    	KeyGenerator keyGen = KeyGenerator.getInstance("DES");
	    	SecureRandom seedRandom = SecureRandom.getInstance("SHA1PRNG", "SUN");
	    	seedRandom.setSeed(seed);
	    	keyGen.init(56, seedRandom);
	    	Key key = keyGen.generateKey();
	    	Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
	    	cipher.init(Cipher.DECRYPT_MODE, key);
	    	return cipher.doFinal(fileBytes);
		} catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException | 
        		BadPaddingException | IllegalBlockSizeException | NoSuchProviderException e) {
			System.out.println("SecureFolder getFileEncryptedBytes(): ERROR opening file '" + filePath + "'. " + e.getMessage());
			return null;
		}
	}
	
	private byte[] getFileEnvelopeBytes (String filePath) {
		byte[] fileBytes = this.fileHandle.getBytes(filePath);
		if (fileBytes == null) {
			return null;
		}
		try {
	    	Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
	    	cipher.init(Cipher.DECRYPT_MODE, this.privateKey);
	    	byte[] seed = cipher.doFinal(fileBytes);
	    	return seed;
    	} catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException | 
        		BadPaddingException | IllegalBlockSizeException e) {
    		System.out.println("FileSecure getFileEnvelopeBytes(): ERROR opening file '" + filePath + "'. " + e.getMessage());
    		return null;
    	}
	}
	
	private byte[] getFileSignatureBytes (String filePath) {
		byte[] fileBytes = this.fileHandle.getBytes(filePath);
		if (fileBytes == null) {
			return null;
		}
		return fileBytes;
	}
	
	private boolean validate (byte[] file, byte[] signature) {
	    try {
		    Signature sig = Signature.getInstance("MD5WithRSA");
		    sig.initVerify(this.publicKey);
		    sig.update(file);
			return sig.verify(signature);
		} catch (SignatureException | InvalidKeyException | NoSuchAlgorithmException e) {
			System.out.println("SecureFolder validate(): " + e.getMessage());
			return false;
		}
	}
	
}
