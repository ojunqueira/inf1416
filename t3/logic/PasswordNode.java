package logic;

public class PasswordNode {
	
	private char value;
	private PasswordNode parent;
	private PasswordNode leftNode;
	private PasswordNode rightNode;
	
	public PasswordNode () {
		this.value = '\u0000';
		this.parent = null;
		this.leftNode = null;
		this.rightNode = null;
	}
	
	public PasswordNode getLeftNode () {
		return this.leftNode;
	}
	
	public PasswordNode getParent () {
		return this.parent;
	}
	
	public PasswordNode getRightNode () {
		return this.rightNode;
	}
	
	public char getValue () {
		return this.value;
	}
	
	public void setLeftNode (PasswordNode node) {
		this.leftNode = node;
		node.setParent(this);
	}
	
	public void setRightNode (PasswordNode node) {
		this.rightNode = node;
		node.setParent(this);
	}
	
	public void setValue (char value) {
		this.value = value;
	}
	
	private void setParent (PasswordNode node) {
		this.parent = node;
	}
	
}
