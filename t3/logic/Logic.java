package logic;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;

import utils.Keys;
import utils.Hash;
import utils.Salt;

public class Logic {
	
	private Database jdbc;
	private SecureFolder secureFolder;
	private Keys keys;
	private String userLogin;
	private PrivateKey userPrivateKey;
	
	public Logic () {
		this.jdbc = new Database();
		this.secureFolder = new SecureFolder(this);
		this.keys = new Keys();
		this.userLogin = new String();
		this.userPrivateKey = null;
		
		if (!this.jdbc.connect()) {
			System.exit(1);
		}
		this.jdbc.setLogMessage(1001);
		this.jdbc.setLogMessage(2001);
	}
	
	public boolean addUser (String login, String name, String group, String password, String publicKeyPath) {
		if (this.jdbc.existsLogin(login)) {
			return false;
		}		
		Salt saltCalculator = new Salt();
		String userSalt = saltCalculator.getSalt();
		
		Hash passwordHash = new Hash();
		String userPassword = passwordHash.getHash(password + userSalt);
		
		Integer userPrivilege = this.jdbc.getGroupId(group);
		
		PublicKey userPublicKey = this.keys.getPublicKey(publicKeyPath);
		byte[] userPublicKeyBytes = this.keys.getPublicKeyBytes(userPublicKey);
		if (userPublicKeyBytes == null) {
			return false;
		}
		if (!this.jdbc.setNewUser(login, name, userPrivilege, userPassword, userPublicKeyBytes, userSalt)) {
			return false;
		}
		return true;
	}
	
	public void close () {
		this.secureFolder.deleteOpenedFiles();
		this.jdbc.setLogMessage(1002);
		this.jdbc.disconnect();
	}
	
	public int getAccessCount () {
		return this.jdbc.getUserAccessCount(this.userLogin);
	}
	
	public int getConsultsCount () {
		return this.jdbc.getUserConsultsCount(this.userLogin);
	}
	
	public String getGroup () {
		return this.jdbc.getUserGroup(this.userLogin);
	}
	
	public String[] getAvaiableGroups () {
		return this.jdbc.getGroupsList();
	}

	public ArrayList<String> getFolderListFileNames () {
		return this.secureFolder.getFilesName();
	}
	
	public ArrayList<String> getFolderListFileSecretNames () {
		return this.secureFolder.getSecretFilesName();
	}
	
	public ArrayList<String> getFolderListFileStatus () {
		return this.secureFolder.getFilesStatus();
	}
	
	public String getLogin () {
		return this.userLogin;
	}
	
	public String getName () {
		return this.jdbc.getUserName(this.userLogin);
	}
	
 	public int getPasswordAttemptsLeft () {
		return (3 - this.jdbc.getUserPasswordAttempts(this.userLogin));
	}
	
	public int getPrivateKeyAttemptsLeft () {
		return (3 - this.jdbc.getUserPrivateKeyAttempts(this.userLogin));
	}
	
	public int getUsersCount () {
		return this.jdbc.getUsersCount();
	}
	
	public boolean isUserBlocked (String userLogin) {
		if (this.jdbc.getUserPasswordAttempts(userLogin) >= 3 ||
				this.jdbc.getUserPrivateKeyAttempts(userLogin) >= 3
				) {
				return true;
			}
			return false;
	}
	
	public boolean isUserCadastred (String userLogin) {
		return this.jdbc.existsLogin(userLogin);
	}
	
	public boolean openFile (String name) {
		return this.secureFolder.openFile(name);
	}
	
	public boolean openFolder (String path) {
		return this.secureFolder.openFolder(
				path,
				this.userPrivateKey, 
				this.keys.getPublicKey(this.jdbc.getUserPublicKey(this.userLogin)));
	}
	
	public boolean setLogin (String userLogin) {
		if (!this.jdbc.existsLogin(userLogin)) {
			this.jdbc.setLogMessage(2005, userLogin);
			return false;
		}
		if (this.isUserBlocked(userLogin)) {
			this.jdbc.setLogMessage(2004, userLogin);
			return false;
		}
		this.userLogin = userLogin;
		this.jdbc.setLogMessage(2003, userLogin);
		this.jdbc.setLogMessage(2002);
		this.jdbc.setLogMessage(3001, userLogin);
		return true;
	}
	
	public boolean setPassword (String userPassword) {
		if (this.isUserBlocked(this.userLogin)) {
			return false;
		}
		PasswordVerifier p = new PasswordVerifier();
		if (!p.isValid(this.jdbc.getUserPassword(this.userLogin), this.jdbc.getUserSalt(this.userLogin), userPassword)) {
			this.jdbc.setLogMessage(3004, this.userLogin);
			Integer attempts = this.jdbc.getUserPasswordAttempts(this.userLogin);
			if (attempts == 1) {
				this.jdbc.setLogMessage(3005, this.userLogin);
			} else if (attempts == 2) {
				this.jdbc.setLogMessage(3006, this.userLogin);
			} else if (attempts == 3) {
				this.jdbc.setLogMessage(3007, this.userLogin);
				this.jdbc.setLogMessage(3008, this.userLogin);
				this.userLogin = null;
			} else {
				System.out.println("Logic setPassword(): Maximum password attempts should be less than 4");
			}
			return false;
		}
		this.jdbc.setLogMessage(3003, this.userLogin);
		this.jdbc.setLogMessage(3002, this.userLogin);
		this.jdbc.setLogMessage(4001, this.userLogin);
		return true;
	}
	
	public boolean setPrivateKey (String filePath, String seed) {
		if (this.isUserBlocked(this.userLogin)) {
			return false;
		}
		PrivateKey privateKey = this.keys.getPrivateKey(filePath, seed);
		PublicKey publicKey = this.keys.getPublicKey(this.jdbc.getUserPublicKey(this.userLogin));
		if (privateKey == null || publicKey == null || !this.keys.validateKeys(privateKey, publicKey)) {
			Integer attempts = this.jdbc.getUserPrivateKeyAttempts(this.userLogin);
			if (attempts == 0) {
				this.jdbc.setLogMessage(4004, this.userLogin);
			} else if (attempts == 1) {
				this.jdbc.setLogMessage(4005, this.userLogin);
			} else if (attempts == 2) {
				this.jdbc.setLogMessage(4006, this.userLogin);
				this.jdbc.setLogMessage(4007, this.userLogin);
				this.userLogin = null;
			} else {
				System.out.println("Logic setPrivateKey(): Maximum private key attempts should be less than 4");
			}
			return false;
		}
		this.userPrivateKey = privateKey;
		this.jdbc.setLogMessage(4003, this.userLogin);
		this.jdbc.setLogMessage(4002, this.userLogin);
		return true;
	}
	
	public void setLogMessage (Integer message) {
		if (this.userLogin.isEmpty()) {
			this.jdbc.setLogMessage(message);
		} else {
			this.jdbc.setLogMessage(message, this.userLogin);
		}
	}
	
	public void setLogMessage (Integer message, String fileName) {
			this.jdbc.setLogMessage(message, this.userLogin, fileName);
	}
}
