package logic;

import utils.Hash;

public class PasswordVerifier {
	
	Hash hash;
	private PasswordNode root;
	boolean valid;
	
	public PasswordVerifier () {
		this.hash = new Hash();
		this.root = new PasswordNode();
		this.valid = false;
	}
	
	public boolean isValid (String passwordHash, String passwordSalt, String passwordVector) {
		this.populateTree(passwordVector);
		this.matchPassword(passwordHash, passwordSalt);		
		return this.valid;
	}
		
	private void matchPassword (String passwordHash, String passwordSalt) {
		this.matchPasswordNode(this.root.getLeftNode(), new String(), passwordHash, passwordSalt);
		this.matchPasswordNode(this.root.getRightNode(), new String(), passwordHash, passwordSalt);
	}
	
	private void matchPasswordNode (PasswordNode node, String previousValues, String passwordHash, String passwordSalt) {
		previousValues = previousValues + node.getValue();
		if (!(node.getLeftNode() == null)) {
			this.matchPasswordNode(node.getLeftNode(), previousValues, passwordHash, passwordSalt);
			this.matchPasswordNode(node.getRightNode(), previousValues, passwordHash, passwordSalt);
		} else {
			String passwordPlusSaltHash = this.hash.getHash(previousValues + passwordSalt);
			if (passwordPlusSaltHash.equals(passwordHash)) {
				this.valid = true;
			}
		}
	}
	
	private void populateNodeLeafs (PasswordNode node, char value1, char value2) {
		if (node.getLeftNode() == null) {
			PasswordNode leftNode = new PasswordNode();
			leftNode.setValue(value1);
			node.setLeftNode(leftNode);
			
			PasswordNode rightNode = new PasswordNode();
			rightNode.setValue(value2);
			node.setRightNode(rightNode);
		} else {
			this.populateNodeLeafs(node.getLeftNode(), value1, value2);
			this.populateNodeLeafs(node.getRightNode(), value1, value2);
		}
	}
	
	private void populateTree (String password) {
		for (int i = 0; i < password.length(); i = i + 2) {
			this.populateNodeLeafs(this.root, password.charAt(i), password.charAt(i + 1));
		}
	}
	
}
