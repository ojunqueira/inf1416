package logic;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Calendar;

import utils.Keys;
import utils.Hash;
import utils.Salt;

public class Database {
	
	private static Connection con;
	
	public Database () {
		this.connect();
		//this.reset(); // ONLY FOR TEST
	}
	
	public boolean connect () {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://127.0.0.1/test";
			Database.con = (Connection) DriverManager.getConnection(url, "root", new String());
			return true;
		} catch (SQLException | ClassNotFoundException e) {
			System.out.println("Database connect(): " + e.getMessage());
			return false;
		}
	}
	
	public void disconnect () {
		try {
			Database.con.close();
		} catch (SQLException e) {
			System.out.println("Database disconnect(): " + e.getMessage());
		}
	}
	
	public boolean existsLogin (String userLogin) {
		try {
			String sql = "SELECT 1 FROM users WHERE us_login = '" + userLogin + "';";
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery(sql);
			while(res.next()) {
				return true;
			}
			return false;
		} catch (SQLException e) {
			System.out.println("Database existsLogin(): " + e.getMessage());
			return false;
		}
	}

	public Integer getGroupId (String group) {
		try {
			String sql = "SELECT gr_id FROM groups WHERE gr_name = '" + group + "';";
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery(sql);
			while(res.next()) {
				return res.getInt("gr_id");
			}
		} catch (SQLException e) {
			System.out.println("Database getGroupId(): " + e.getMessage());
		}
		return null;
	}
	
	public String[] getGroupsList () {
		try {
			String sql = "SELECT gr_name FROM groups;";
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery(sql);
			res.last();
			int count = res.getRow();
			res.beforeFirst();
			String[] groups = new String[count];
			while (res.next()) {
				groups[res.getRow() - 1] = res.getString("gr_name");
			}
			return groups;
		} catch (SQLException e) {
			System.out.println("Database getGroupsList(): " + e.getMessage());
		}
		return null;
	}
	
	public Integer getUserAccessCount (String userLogin) {
		try {
			String sql = "SELECT 1 FROM log WHERE lg_login = '" + userLogin + "' AND lg_message = '" + 4003 + "';";
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery(sql);
			res.last();
			return res.getRow();
		} catch (SQLException e) {
			System.out.println("Database getUserAccessCount(): " + e.getMessage());
		}
		return null;
	}

	public Integer getUserConsultsCount (String userLogin) {
		try {
			String sql = "SELECT 1"
					+ " FROM log"
					+ " WHERE lg_login = '" + userLogin + "'"
					+ " AND lg_file = 'index'"
					+ " AND lg_message in ('8005', '8007')"
					+ ";";
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery(sql);
			res.last();
			return res.getRow();
		} catch (SQLException e) {
			System.out.println("Database getUserConsultsCount(): " + e.getMessage());
		}
		return null;
	}
	
	public String getUserGroup (String userLogin) {
		try {
			String sql = "SELECT gr_name "
					+ "FROM users U, groups G "
					+ "WHERE U.us_login = '" + userLogin + "'"
					+ "AND G.gr_id = U.us_privilege"
					+ ";";
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery(sql);
			while(res.next()) {
				return res.getString("gr_name");
			}
		} catch (SQLException e) {
			System.out.println("Database getUserGroup(): " + e.getMessage());
		}
		return null;
	}
	
	public String getUserName (String userLogin) {
		try {
			String sql = "SELECT us_name FROM users WHERE us_login = '" + userLogin + "';";
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery(sql);
			while(res.next()) {
				return res.getString("us_name");
			}
		} catch (SQLException e) {
			System.out.println("Database getUserName(): " + e.getMessage());
		}
		return null;
	}
	
	public String getUserPassword (String userLogin) {
		try {
			String sql = "SELECT us_password FROM users WHERE us_login = '" + userLogin + "';";
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery(sql);
			while(res.next()) {
				return res.getString("us_password");
			}
		} catch (SQLException e) {
			System.out.println("Database getUserPassword(): " + e.getMessage());
		}
		return null;
	}
	
	public Integer getUserPasswordAttempts (String userLogin) {
		// 120000 miliseconds = 2 minutes
		Timestamp twoMinutesAgoTimestamp = new Timestamp(Calendar.getInstance().getTime().getTime() - 120000);
		try {
			String sql = "SELECT 1 "
					+ "FROM log "
					+ "WHERE lg_login = '" + userLogin + "'"
					+ "AND lg_message = '" + 3004 + "'"
					+ "AND lg_time > '" + twoMinutesAgoTimestamp + "'"
					+ ";";
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery(sql);
			res.last();
			return res.getRow();
		} catch (SQLException e) {
			System.out.println("Database getUserPasswordAttempts(): " + e.getMessage());
		}
		return null;
	}
	
	public Integer getUserPrivateKeyAttempts (String userLogin) {
		// 120000 miliseconds = 2 minutes
		Timestamp twoMinutesAgoTimestamp = new Timestamp(Calendar.getInstance().getTime().getTime() - 120000);
		try {
			String sql;
			Statement st;
			ResultSet res;
			sql = "SELECT 1 "
					+ "FROM log "
					+ "WHERE lg_login = '" + userLogin + "'"
					+ "AND lg_message = '" + 4006 + "'"
					+ "AND lg_time > '" + twoMinutesAgoTimestamp + "'"
					+ ";";
			st = con.createStatement();
			res = st.executeQuery(sql);
			while(res.next()) {
				return 3;
			}
			sql = "SELECT 1 "
					+ "FROM log "
					+ "WHERE lg_login = '" + userLogin + "'"
					+ "AND lg_message = '" + 4005 + "'"
					+ "AND lg_time > '" + twoMinutesAgoTimestamp + "'"
					+ ";";
			st = con.createStatement();
			res = st.executeQuery(sql);
			while(res.next()) {
				return 2;
			}
			sql = "SELECT 1 "
					+ "FROM log "
					+ "WHERE lg_login = '" + userLogin + "'"
					+ "AND lg_message = '" + 4004 + "'"
					+ "AND lg_time > '" + twoMinutesAgoTimestamp + "'"
					+ ";";
			st = con.createStatement();
			res = st.executeQuery(sql);
			while(res.next()) {
				return 1;
			}
			return 0;
		} catch (SQLException e) {
			System.out.println("Database getUserPrivateKeyAttempts(): " + e.getMessage());
		}
		return null;
	}
	
	public byte[] getUserPublicKey (String userLogin) {
		try {
			String sql = "SELECT us_pubkey FROM users WHERE us_login = '" + userLogin + "';";
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery(sql);
			while(res.next()) {
				return res.getBytes("us_pubkey");
			}
		} catch (SQLException e) {
			System.out.println("Database getUserPublicKey(): " + e.getMessage());
		}
		return null;
	}
	
	public String getUserSalt (String userLogin) {
		try {
			String sql = "SELECT us_salt FROM users WHERE us_login = '" + userLogin + "';";
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery(sql);
			while(res.next()) {
				return res.getString("us_salt");
			}
		} catch (SQLException e) {
			System.out.println("Database getUserSalt(): " + e.getMessage());
		}
		return null;
	}
	
	public Integer getUsersCount () {
		try {
			String sql = "SELECT 1 FROM users;";
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery(sql);
			res.last();
			return res.getRow();
		} catch (SQLException e) {
			System.out.println("Database getUsersCount(): " + e.getMessage());
		}
		return null;
	}
		
	public void setLogMessage (Integer messageId) {
		try {
			Timestamp currentTimestamp = new Timestamp(Calendar.getInstance().getTime().getTime());
			String sql = "INSERT INTO log (lg_message, lg_time) VALUES "
					+ "("
					+ "'" + messageId + "',"
					+ "'" + currentTimestamp + "'"
					+ ");";
			PreparedStatement comandoSQL = Database.con.prepareStatement(sql);
			comandoSQL.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Database setLogMessage(): " + e.getMessage());
		}
	}
	
	public void setLogMessage (Integer messageId, String userLogin) {
		try {
			Timestamp currentTimestamp = new Timestamp(Calendar.getInstance().getTime().getTime());
			String sql = "INSERT INTO log (lg_message, lg_login, lg_time) VALUES "
					+ "("
					+ "'" + messageId + "',"
					+ "'" + userLogin + "',"
					+ "'" + currentTimestamp + "'"
					+ ");";
			PreparedStatement comandoSQL = Database.con.prepareStatement(sql);
			comandoSQL.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Database setLogMessage(): " + e.getMessage());
		}
	}
	
	public void setLogMessage (Integer messageId, String userLogin, String fileName) {
		try {
			Timestamp currentTimestamp = new Timestamp(Calendar.getInstance().getTime().getTime());
			String sql = "INSERT INTO log (lg_message, lg_login, lg_file, lg_time) VALUES "
					+ "("
					+ "'" + messageId + "',"
					+ "'" + userLogin + "',"
					+ "'" + fileName + "',"
					+ "'" + currentTimestamp + "'"
					+ ");";
			PreparedStatement comandoSQL = Database.con.prepareStatement(sql);
			comandoSQL.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Database setLogMessage(): " + e.getMessage());
		}
	}
	
	
	public boolean setNewUser (String userLogin, String userName, Integer userPrivilege, String userPassword, byte[] userPublicKey, String userSalt) {
		try {
			String sql = "INSERT INTO users (us_login, us_name, us_privilege, us_password, us_pubkey, us_salt) "
					+ "VALUES (?, ?, ?, ?, ?, ?);";
			PreparedStatement comandoSQL = Database.con.prepareStatement(sql);
			comandoSQL.setString(1, userLogin);
			comandoSQL.setString(2, userName);
			comandoSQL.setInt(3, userPrivilege);
			comandoSQL.setString(4, userPassword);
			comandoSQL.setBytes(5, userPublicKey);
			comandoSQL.setString(6, userSalt);
			comandoSQL.executeUpdate();
			return true;
		} catch (SQLException e) {
			System.out.println("Database setNewUser(): " + e.getMessage());
			return false;
		}
	}
	
	
	///////////////////////////////////////////////////////////
	// REFACTORING DATABASE ONLY:
	///////////////////////////////////////////////////////////
	
	private void reset () {
		this.dropForeignKeys();
		
		this.clearTableGroups();
		this.clearTableLog();
		this.clearTableMessages();
		this.clearTableUsers();
		
		this.createForeignKeys();
		
		this.populateTableMessages();
		this.populateTableGroups();
		this.populateTableUsers();
	}

	private void clearTableGroups () {
		try {
			String sql_drop = "DROP TABLE groups;";
			PreparedStatement comandoSQL = Database.con.prepareStatement(sql_drop);
			comandoSQL.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Database clearTableGroups(): " + e.getMessage());
		}
		try {
			String sql_create = "CREATE TABLE groups ( "
					+ "gr_id	 		serial 					Not Null, "
					+ "gr_name  		varchar(50) 	Unique	Not Null, "
					+ "constraint pk_privilege Primary Key (gr_id)"
					+ " );";
			PreparedStatement comandoSQL = Database.con.prepareStatement(sql_create);
			comandoSQL.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Database clearTableGroups(): " + e.getMessage());
		}
	}
	
	private void clearTableLog () {
		try {
			String sql_drop = "DROP TABLE log;";
			PreparedStatement comandoSQL = Database.con.prepareStatement(sql_drop);
			comandoSQL.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Database clearTableLog(): " + e.getMessage());
		}
		try {
			String sql_create = "CREATE TABLE log ( "
					+ "lg_id 			serial 			Not Null, "
					+ "lg_message  		integer		 	Not Null, "
					+ "lg_login 		varchar(20)		Null, "
					+ "lg_file	 		varchar(200) 	Null, "
					+ "lg_time			timestamp 		Not Null, "
					+ "constraint pk_log Primary Key (lg_id)"
					+ " );";
			PreparedStatement comandoSQL = Database.con.prepareStatement(sql_create);
			comandoSQL.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Database clearTableLog(): " + e.getMessage());
		}
	}

	private void clearTableMessages () {
		try {
			String sql_drop = "DROP TABLE messages;";
			PreparedStatement comandoSQL = Database.con.prepareStatement(sql_drop);
			comandoSQL.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Database clearTableMessages(): " + e.getMessage());
		}
		try {
			String sql_create = "CREATE TABLE messages ( "
					+ "mg_id 			integer			Not Null, "
					+ "mg_message  		varchar(200) 	Not Null, "
					+ "constraint pk_message Primary Key (mg_id)"
					+ " );";
			PreparedStatement comandoSQL = Database.con.prepareStatement(sql_create);
			comandoSQL.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Database clearTableMessages(): " + e.getMessage());
		}
	}
	
	private void clearTableUsers () {
		try {
			String sql_drop = "DROP TABLE users;";
			PreparedStatement comandoSQL = Database.con.prepareStatement(sql_drop);
			comandoSQL.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Database clearTableUser(): " + e.getMessage());
		}
		try {
			String sql_create = "CREATE TABLE users ( "
					+ "us_login 		varchar(20) 	Not Null, "
					+ "us_name  		varchar(50) 	Not Null, "
					+ "us_privilege		bigint unsigned Not Null, "
					+ "us_password 		varchar(32) 	Not Null, "
					+ "us_pubkey		varchar(256) 	Not Null, "
					+ "us_salt			varchar(10) 	Not Null, "
					+ "constraint pk_user Primary Key (us_login)"
					+ " );";
			PreparedStatement comandoSQL = Database.con.prepareStatement(sql_create);
			comandoSQL.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Database clearTableUser(): " + e.getMessage());
		}
	}
	
	private void createForeignKeys () {
		try {
			String sql;
			PreparedStatement comandoSQL;
			sql = "ALTER TABLE users ADD CONSTRAINT fk_user_privilege FOREIGN KEY (us_privilege) REFERENCES groups (gr_id);";
			comandoSQL = Database.con.prepareStatement(sql);
			comandoSQL.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Database createForeignKeys(): " + e.getMessage());
		}
		try {
			String sql;
			PreparedStatement comandoSQL;
			sql = "ALTER TABLE log ADD CONSTRAINT fk_log_message FOREIGN KEY (lg_message) REFERENCES messages (mg_id);";
			comandoSQL = Database.con.prepareStatement(sql);
			comandoSQL.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Database createForeignKeys(): " + e.getMessage());
		}
	}
	
	private void dropForeignKeys () {
		try {
			String sql;
			PreparedStatement comandoSQL;
			sql = "ALTER TABLE users DROP FOREIGN KEY fk_user_privilege;";
			comandoSQL = Database.con.prepareStatement(sql);
			comandoSQL.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Database dropForeignKeys(): " + e.getMessage());
		}
		try {
			String sql;
			PreparedStatement comandoSQL;
			sql = "ALTER TABLE log DROP FOREIGN KEY fk_log_message;";
			comandoSQL = Database.con.prepareStatement(sql);
			comandoSQL.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Database dropForeignKeys(): " + e.getMessage());
		}
	}
	
	private void populateTableMessages () {
		try {
			String sql;
			PreparedStatement comandoSQL;
			sql = "INSERT INTO messages (mg_id, mg_message) VALUES "
					+ "('1001', 'Sistema iniciado.'), "
					+ "('1002', 'Sistema encerrado.'), "
					+ "('2001', 'Autenticação etapa 1 iniciada.'), "
					+ "('2002', 'Autenticação etapa 1 encerrada.'), "
					+ "('2003', 'Login name <login_name> identificado com acesso liberado.'), "
					+ "('2004', 'Login name <login_name> identificado com acesso bloqueado.'), "
					+ "('2005', 'Login name <login_name> não identificado.'), "
					+ "('3001', 'Autenticação etapa 2 iniciada para <login_name>.'), "
					+ "('3002', 'Autenticação etapa 2 encerrada para <login_name>.'), "
					+ "('3003', 'Senha pessoal verificada positivamente para <login_name>.'), "
					+ "('3004', 'Senha pessoal verificada negativamente para <login_name>.'), "
					+ "('3005', 'Primeiro erro da senha pessoal contabilizado para <login_name>.'), "
					+ "('3006', 'Segundo erro da senha pessoal contabilizado para <login_name>.'), "
					+ "('3007', 'Terceiro erro da senha pessoal contabilizado para <login_name>.'), "
					+ "('3008', 'Acesso do usuario <login_name> bloqueado pela autenticação etapa 2.'), "
					+ "('4001', 'Autenticação etapa 3 iniciada para <login_name>.'), "
					+ "('4002', 'Autenticação etapa 3 encerrada para <login_name>.'), "
					+ "('4003', 'Chave privada verificada positivamente para <login_name>.'), "
					+ "('4004', 'Primeiro erro de chave privada contabilizado para <login_name>.'), "
					+ "('4005', 'Segundo erro de chave privada contabilizado para <login_name>.'), "
					+ "('4006', 'Terceiro erro de chave privada contabilizado para <login_name>.'), "
					+ "('4007', 'Acesso do usuario <login_name> bloqueado pela autenticação etapa 3.'), "
					+ "('5001', 'Tela principal apresentada para <login_name>.'), "
					+ "('5002', 'Opção 1 do menu principal selecionada por <login_name>.'), "
					+ "('5003', 'Opção 2 do menu principal selecionada por <login_name>.'), "
					+ "('5004', 'Opção 3 do menu principal selecionada por <login_name>.'), "
					+ "('5005', 'Opção 4 do menu principal selecionada por <login_name>.'), "
					+ "('6001', 'Tela de cadastro apresentada para <login_name>.'), "
					+ "('6002', 'Botão cadastrar pressionado por <login_name>.'), "
					+ "('6003', 'Botão voltar de cadastrar para o menu principal pressionado por <login_name>.'), "
					+ "('7001', 'Tela de alteração apresentada para <login_name>.'), "
					+ "('7002', 'Botão alterar pressionado por <login_name>.'), "
					+ "('7003', 'Botão voltar de alterar para o menu principal pressionado por <login_name>.'), "
					+ "('8001', 'Tela de consulta apresentada para <login_name>.'), "
					+ "('8002', 'Botão voltar de consultar para o menu principal pressionado por <login_name>.'), "
					+ "('8003', 'Arquivo <arq_name> selecionado por <login_name> para decriptação.'), "
					+ "('8004', 'Arquivo <arq_name> decriptado com sucesso para <login_name>.'), "
					+ "('8005', 'Arquivo <arq_name> verificado com sucesso para <login_name>.'), "
					+ "('8006', 'Falha na decriptação do arquivo <arq_name> para <login_name>.'), "
					+ "('8007', 'Falha na verificação do arquivo <arq_name> para <login_name>.'), "
					+ "('9001', 'Tela de saída apresentada para <login_name>.'), "
					+ "('9002', 'Botão sair pressionado por <login_name>.'), "
					+ "('9003', 'Botão voltar de sair para o menu principal pressionado por <login_name>.'), "
					+ "('9004', 'Arquivos decriptados apagados com sucesso para <login_name>.'), "
					+ "('9005', 'Arquivo decriptado <arq_name> não pôde ser apagado para <login_name>.') "
					+ ";";
			comandoSQL = Database.con.prepareStatement(sql);
			comandoSQL.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Database populateTableMessage(): " + e.getMessage());
		}
	}
	
	private void populateTableGroups () {
		try {
			String sql;
			PreparedStatement comandoSQL;
			sql = "INSERT INTO groups (gr_name) VALUES "
					+ "('administrador'), "
					+ "('usuario') "
					+ ";";
			comandoSQL = Database.con.prepareStatement(sql);
			comandoSQL.executeUpdate();

		} catch (SQLException e) {
			System.out.println("Database populateTableGroups(): " + e.getMessage());
		}
	}
	
	private void populateTableUsers () {
		try {
			String sql;
			PreparedStatement comandoSQL;
			sql = "INSERT INTO users (us_login, us_name, us_privilege, us_password, us_pubkey, us_salt) "
					+ "VALUES (?, ?, ?, ?, ?, ?);";
			comandoSQL = Database.con.prepareStatement(sql);
			
			Salt saltCalculator = new Salt();
			Hash passwordHash = new Hash();
			Keys keys = new Keys();
			
			String userLogin = "admin";
			String userName = "Administrador Raiz";
			Integer userPrivilege = this.getGroupId("administrador");
			String userSalt = saltCalculator.getSalt();
			String userPassword = passwordHash.getHash("192837" + userSalt);
			byte[] userPublicKey = keys.getPublicKeyBytes(keys.getPublicKey("/Users/otavio/Google Drive/pucSistemas/14_2_segurancaInformacao/t3/exemplo/Keys/userpub"));
			
			comandoSQL.setString(1, userLogin);
			comandoSQL.setString(2, userName);
			comandoSQL.setInt(3, userPrivilege);
			comandoSQL.setString(4, userPassword);
			comandoSQL.setBytes(5, userPublicKey);
			comandoSQL.setString(6, userSalt);
			comandoSQL.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Database populateTableUser(): " + e.getMessage());
		}
	}
	
}
