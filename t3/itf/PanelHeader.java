package itf;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JPanel;

import logic.Logic;

@SuppressWarnings("serial")
public class PanelHeader extends JPanel {
	private Logic logic;
	
	private JLabel txtUserGroup;
	private JLabel txtUserLogin;
	private JLabel txtUserName;
	
	public PanelHeader (Logic logic) {
		this.logic = logic;
		this.setLayout(null);
		this.setBackground(new Color(234, 82, 68));
		
		JLabel lbl_login = new JLabel("Login:");
		lbl_login.setBounds(50, 10, 50, 30);
		this.add(lbl_login);
		this.txtUserLogin = new JLabel();
		this.txtUserLogin.setBounds(100, 10, 100, 30);
		this.add(this.txtUserLogin);
		
		JLabel lbl_group = new JLabel("Group:");
		lbl_group.setBounds(220, 10, 50, 30);
		this.add(lbl_group);
		this.txtUserGroup = new JLabel();
		this.txtUserGroup.setBounds(270, 10, 100, 30);
		this.add(this.txtUserGroup);
		
		JLabel lbl_name = new JLabel("Name:");
		lbl_name.setBounds(390, 10, 50, 30);
		this.add(lbl_name);
		this.txtUserName = new JLabel();
		this.txtUserName.setBounds(440, 10, 100, 30);
		this.add(this.txtUserName);
	}
	
	public void update () {
		this.txtUserGroup.setText(this.logic.getGroup());
		this.txtUserLogin.setText(this.logic.getLogin());
		this.txtUserName.setText(this.logic.getName());
	}
}
