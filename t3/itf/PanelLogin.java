package itf;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import utils.RandNoRepeat;
import logic.Logic;


@SuppressWarnings("serial")
public class PanelLogin extends JPanel implements ActionListener {
	
	private Dialog controller;
	private Logic logic;
	
	private JLabel lblLogin;
	private JTextField txtLogin;
	private JButton btnLoginOk;
	
	private JButton btnPassword1;
	private JButton btnPassword2;
	private JButton btnPassword3;
	private JButton btnPassword4;
	private JButton btnPassword5;
	private JTextField txtPassword;
	private String password;

	private JLabel lblPrivateKeyPath;
	private JTextField txtPrivateKeyPath;
	private JLabel lblPrivateKeySeed;
	private JTextField txtPrivateKeySeed;
	private JButton btnPrivateKeyOk;
	
	private JLabel errorMessage;
	private JButton btn_exit;
	
	public PanelLogin (Logic logic, Dialog dialog) {
		this.controller = dialog;
		this.logic = logic;
		this.setLayout(null);
		
		this.lblLogin = new JLabel("User:");
		this.lblLogin.setBounds(105, 30, 70, 40);
		this.add(lblLogin);
		this.txtLogin = new JTextField ();
		this.txtLogin.setBounds(195, 30, 200, 40);
		this.add(this.txtLogin);
		this.btnLoginOk = new JButton("Ok");
		this.btnLoginOk.setBounds(415, 30, 80, 40);
		this.btnLoginOk.addActionListener(this);
		this.add(this.btnLoginOk);
		
		this.btnPassword1 = new JButton ();
		this.btnPassword1.setBounds(60, 110, 80, 80);
		this.btnPassword1.addActionListener(this);
		this.add(this.btnPassword1);
		this.btnPassword2 = new JButton ();
		this.btnPassword2.setBounds(160, 110, 80, 80);
		this.btnPassword2.addActionListener(this);
		this.add(this.btnPassword2);
		this.btnPassword3 = new JButton ();
		this.btnPassword3.setBounds(260, 110, 80, 80);
		this.btnPassword3.addActionListener(this);
		this.add(this.btnPassword3);
		this.btnPassword4 = new JButton ();
		this.btnPassword4.setBounds(360, 110, 80, 80);
		this.btnPassword4.addActionListener(this);
		this.add(this.btnPassword4);
		this.btnPassword5 = new JButton ();
		this.btnPassword5.setBounds(460, 110, 80, 80);
		this.btnPassword5.addActionListener(this);
		this.add(this.btnPassword5);
		this.txtPassword = new JTextField ();
		this.txtPassword.setBounds(200, 210, 200, 40);
		this.add(this.txtPassword);
		this.password = new String();
		
		this.lblPrivateKeyPath = new JLabel("Caminho da chave privada:");
		this.lblPrivateKeyPath.setBounds(90, 270, 400, 40);
		this.add(this.lblPrivateKeyPath);
		this.txtPrivateKeyPath = new JTextField("/Users/otavio/Google Drive/pucSistemas/14_2_segurancaInformacao/t3/exemplo/Keys/userpriv"); // APAGAR
		this.txtPrivateKeyPath.setBounds(50, 310, 400, 40);
		this.add(this.txtPrivateKeyPath);
		this.lblPrivateKeySeed = new JLabel("Frase secreta:");
		this.lblPrivateKeySeed.setBounds(90, 360, 400, 40);
		this.add(this.lblPrivateKeySeed);
		this.txtPrivateKeySeed = new JTextField("segredo"); // APAGAR
		this.txtPrivateKeySeed.setBounds(50, 400, 400, 40);
		this.add(this.txtPrivateKeySeed);
		this.btnPrivateKeyOk = new JButton("Ok");
		this.btnPrivateKeyOk.setBounds(470, 355, 80, 40);
		this.btnPrivateKeyOk.addActionListener(this);
		this.add(this.btnPrivateKeyOk);
		
		this.errorMessage = new JLabel("");
		this.errorMessage.setBounds(50, 460, 500, 40);
		this.errorMessage.setForeground(Color.RED);
		this.add(this.errorMessage);
		
		this.btn_exit = new JButton("Exit");
		this.btn_exit.setBounds(240, 530, 120, 40);
		this.btn_exit.addActionListener(this);
		this.add(this.btn_exit);
		
		this.setStateWaitingUser();
	}
	
	public void actionPerformed (ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			JButton btn = (JButton) e.getSource();
			if (btn.equals(this.btn_exit)) {
				this.callbackButtonExit();
			} else if (btn.equals(this.btnLoginOk)) {
				this.callbackButtonLoginOk();
			} else if (btn.equals(this.btnPrivateKeyOk)) {
				this.callbackButtonPrivateKeyOk();
			} else {
				this.callbackButtonPasswordClick(btn.getText());
			}
		}
		
	}
	
	private void callbackButtonExit () {
		this.controller.close();
	}
	
	private void callbackButtonLoginOk () {
		if (!this.logic.setLogin(this.txtLogin.getText())) {
			if (!this.logic.isUserCadastred(this.txtLogin.getText())) {
				this.errorMessageUpdate("User '" + this.txtLogin.getText() + "' was not found on database.");
			} else if (this.logic.isUserBlocked(this.txtLogin.getText())) {
				this.errorMessageUpdate("User '" + this.txtLogin.getText() + "' is blocked.");
			} else {
				this.errorMessageUpdate("Unknown error verifying user '" + this.txtLogin.getText() + "'.");
			}
			this.txtLogin.grabFocus();
			return;
		}
		this.errorMessageClear();
		this.setStateWaitingPassword();
	}
	
	private void callbackButtonPasswordClick (String buttonTitle) {
		String[] nums = buttonTitle.split(" ");
		this.txtPassword.setText(this.txtPassword.getText() + "x");
		this.password = this.password + nums[0] + nums[2];
		if (this.password.length() < 12) {
			this.updatePasswordButtonsTitle();
			return;
		} 
		if (!this.logic.setPassword(this.password)) {
			this.password = "";
			this.txtPassword.setText("");
			if (this.logic.isUserBlocked(this.txtLogin.getText())) {
				this.errorMessageUpdate("Access of user '" + this.txtLogin.getText() + "' blocked due to wrong password attempts.");
				this.setStateWaitingUser();
			} else {
				this.errorMessageUpdate("Wrong password. You have " + this.logic.getPasswordAttemptsLeft() + " more attempts.");
			}
			return;
		}
		this.errorMessageClear();
		this.setStateWaitingPrivKey();
	}
	
	private void callbackButtonPrivateKeyOk () {
		if (this.txtPrivateKeyPath.getText().equals("") || this.txtPrivateKeySeed.getText().equals("")) {
			this.errorMessageUpdate("Private key path and private key seed must be filled.");
			return;
		}
		if (!this.logic.setPrivateKey(this.txtPrivateKeyPath.getText(), this.txtPrivateKeySeed.getText())) {
			if (this.logic.isUserBlocked(this.txtLogin.getText())) {
				this.errorMessageUpdate("Access of user '" + this.txtLogin.getText() + "' blocked due to wrong private key attempts.");
				this.setStateWaitingUser();
			} else {
				this.errorMessageUpdate("Wrong private key. You have " + this.logic.getPrivateKeyAttemptsLeft() + " more attempts.");
				this.txtPrivateKeyPath.grabFocus();
			}
			return;
		}
		this.errorMessageClear();
		this.controller.setStateMain();
	}
		
	private void errorMessageClear () {
		this.errorMessage.setText("");
	}
	
	private void errorMessageUpdate (String message) {
		this.errorMessage.setText(message);
	}
	
	private void setStateWaitingUser () {
		this.txtLogin.setText("");
		this.password = "";
		this.txtPassword.setText("");
		this.txtPrivateKeyPath.setText("");
		this.txtPrivateKeySeed.setText("");
		this.txtLogin.setEditable(true);
		
		this.lblLogin.setVisible(true);
		this.txtLogin.setVisible(true);
		this.btnLoginOk.setVisible(true);
		
		this.btnPassword1.setVisible(false);
		this.btnPassword2.setVisible(false);
		this.btnPassword3.setVisible(false);
		this.btnPassword4.setVisible(false);
		this.btnPassword5.setVisible(false);
		this.txtPassword.setVisible(false);
		
		this.lblPrivateKeyPath.setVisible(false);
		this.txtPrivateKeyPath.setVisible(false);
		this.lblPrivateKeySeed.setVisible(false);
		this.txtPrivateKeySeed.setVisible(false);
		this.btnPrivateKeyOk.setVisible(false);
	}
	
	private void setStateWaitingPassword () {
		this.updatePasswordButtonsTitle();
		this.lblLogin.setVisible(true);
		this.txtLogin.setVisible(true);
		this.txtLogin.setEditable(false);
		this.btnLoginOk.setVisible(false);
		
		this.btnPassword1.setVisible(true);
		this.btnPassword2.setVisible(true);
		this.btnPassword3.setVisible(true);
		this.btnPassword4.setVisible(true);
		this.btnPassword5.setVisible(true);
		this.txtPassword.setVisible(true);
		this.txtPassword.setEditable(false);
		
		this.lblPrivateKeyPath.setVisible(false);
		this.txtPrivateKeyPath.setVisible(false);
		this.lblPrivateKeySeed.setVisible(false);
		this.txtPrivateKeySeed.setVisible(false);
		this.btnPrivateKeyOk.setVisible(false);
	}
	
	private void setStateWaitingPrivKey () {
		this.lblLogin.setVisible(true);
		this.txtLogin.setVisible(true);
		this.txtLogin.setEditable(false);
		this.btnLoginOk.setVisible(false);
		
		this.btnPassword1.setVisible(false);
		this.btnPassword2.setVisible(false);
		this.btnPassword3.setVisible(false);
		this.btnPassword4.setVisible(false);
		this.btnPassword5.setVisible(false);
		this.txtPassword.setVisible(true);
		this.txtPassword.setEditable(false);
		
		this.lblPrivateKeyPath.setVisible(true);
		this.txtPrivateKeyPath.setVisible(true);
		this.lblPrivateKeySeed.setVisible(true);
		this.txtPrivateKeySeed.setVisible(true);
		this.btnPrivateKeyOk.setVisible(true);
	}
	
	private void updatePasswordButtonsTitle () {
		RandNoRepeat rand = new RandNoRepeat();
		btnPassword1.setText(rand.GetNumber() + " or " + rand.GetNumber());
		btnPassword2.setText(rand.GetNumber() + " or " + rand.GetNumber());
		btnPassword3.setText(rand.GetNumber() + " or " + rand.GetNumber());
		btnPassword4.setText(rand.GetNumber() + " or " + rand.GetNumber());
		btnPassword5.setText(rand.GetNumber() + " or " + rand.GetNumber());
	}
	
}
