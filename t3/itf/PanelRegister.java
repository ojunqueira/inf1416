package itf;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import logic.Logic;

@SuppressWarnings("serial")
public class PanelRegister extends JPanel implements ActionListener {
	private Logic logic;
	private Dialog controller;
	
	private JLabel txtUsersCount;
	
	private JTextField txtUserName;
	private JTextField txtUserLogin;
	private JComboBox<String> txtUserGroup;
	private JTextField txtUserPassword;
	private JTextField txtUserPasswordVerify;
	private JTextField txtUserPublicKeyPath;
	
	private JLabel errorMessage;
	private JButton btn_save;
	private JButton btn_return;
	
	public PanelRegister (Logic logic, Dialog dialog) {
		this.logic = logic;
		this.controller = dialog;
		this.setLayout(null);
		this.setBackground(new Color(250, 209, 9));
		
		JLabel lblUsersCount = new JLabel("Users on system:");
		lblUsersCount.setBounds(10, 10, 150, 30);
		this.add(lblUsersCount);
		this.txtUsersCount = new JLabel();
		this.txtUsersCount.setBounds(180, 10, 50, 30);
		this.add(this.txtUsersCount);
		
		JLabel lblUserName = new JLabel("User name:");
		lblUserName.setBounds(50, 80, 150, 30);
		this.add(lblUserName);
		this.txtUserName = new JTextField();
		this.txtUserName.setBounds(250, 80, 300, 30);
		this.add(this.txtUserName);
		
		JLabel lblUserLogin = new JLabel("User login:");
		lblUserLogin.setBounds(50, 120, 150, 30);
		this.add(lblUserLogin);
		this.txtUserLogin = new JTextField();
		this.txtUserLogin.setBounds(250, 120, 300, 30);
		this.add(this.txtUserLogin);
		
		JLabel lblUserGroup = new JLabel("User group:");
		lblUserGroup.setBounds(50, 160, 150, 30);
		this.add(lblUserGroup);
		this.txtUserGroup = new JComboBox<String>(this.logic.getAvaiableGroups());
		this.txtUserGroup.setBounds(250, 160, 300, 30);
		this.txtUserGroup.setEditable(false);
		this.add(this.txtUserGroup);
		
		JLabel lblUserPassword = new JLabel("User password:");
		lblUserPassword.setBounds(50, 200, 150, 30);
		this.add(lblUserPassword);
		this.txtUserPassword = new JTextField();
		this.txtUserPassword.setBounds(250, 200, 300, 30);
		this.add(this.txtUserPassword);
		
		JLabel lblUserPasswordVerify = new JLabel("User password verify:");
		lblUserPasswordVerify.setBounds(50, 240, 150, 30);
		this.add(lblUserPasswordVerify);
		this.txtUserPasswordVerify = new JTextField();
		this.txtUserPasswordVerify.setBounds(250, 240, 300, 30);
		this.add(this.txtUserPasswordVerify);
		
		JLabel lblUserPublicKeyPath = new JLabel("User public key path:");
		lblUserPublicKeyPath.setBounds(50, 280, 150, 30);
		this.add(lblUserPublicKeyPath);
		this.txtUserPublicKeyPath = new JTextField("/Users/otavio/Google Drive/pucSistemas/14_2_segurancaInformacao/t3/exemplo/Keys/userpub");
		this.txtUserPublicKeyPath.setBounds(250, 280, 300, 30);
		this.add(this.txtUserPublicKeyPath);
		
		this.errorMessage = new JLabel();
		this.errorMessage.setBounds(50, 350, 500, 30);
		this.errorMessage.setForeground(Color.RED);
		this.add(this.errorMessage);
		this.btn_save = new JButton("Save");
		this.btn_save.setBounds(10, 510, 90, 30);
		this.btn_save.addActionListener(this);
		this.add(this.btn_save);
		this.btn_return = new JButton("Return");
		this.btn_return.setBounds(110, 510, 90, 30);
		this.btn_return.addActionListener(this);
		this.add(this.btn_return);
	}
	
	public void actionPerformed (ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			JButton btn = (JButton) e.getSource();
			if (btn.equals(this.btn_save)) {
				this.callbackButtonSave();
			} else if (btn.equals(this.btn_return)) {
				this.callbackButtonReturn();
			}
		}
		
	}

	public void update () {
		this.txtUsersCount.setText("" + this.logic.getUsersCount());
	}
	
	private void callbackButtonReturn () {
		this.logic.setLogMessage(6003);	
		this.clear();
		this.controller.setStateMain();
	}
	
	private void callbackButtonSave () {
		this.logic.setLogMessage(6002);
		this.errorMessageClear();
		if (!this.validateFields()) {
			return;
		}
		if (!this.logic.addUser(
				this.txtUserLogin.getText(),
				this.txtUserName.getText(),
				(String) this.txtUserGroup.getSelectedItem(),
				this.txtUserPassword.getText(),
				this.txtUserPublicKeyPath.getText()
				)) {
			if (this.logic.isUserCadastred(this.txtUserLogin.getText())) {
				this.errorMessageUpdate("User login '" + this.txtUserLogin.getText() + "' already exists!");
			} else {
				System.out.println("PanelRegister callbackButtonSave(): Erro ao cadastrar usuario.");
			}
			return;
		}
		this.clear();
		this.controller.updated();
	}
	
	private void clear () {
		this.txtUserGroup.setSelectedIndex(0);
		this.txtUserLogin.setText("");
		this.txtUserName.setText("");
		this.txtUserPassword.setText("");
		this.txtUserPasswordVerify.setText("");
		this.txtUserPublicKeyPath.setText("");
		this.errorMessageClear();
	}
	
	private void errorMessageUpdate (String message) {
		this.errorMessage.setText(message);
	}
	
	private void errorMessageClear () {
		this.errorMessage.setText("");
	}
	
	private boolean validateFields () {
		if (!this.validateUserName()) {
			return false;
		}
		if (!this.validateUserLogin()) {
			return false;
		}
		if (!this.validateUserPassword()) {
			return false;
		}
		if (!this.validateUserPublicKey()) {
			return false;
		}
		return true;
	}
	
	private boolean validateUserName () {
		if (this.txtUserName.getText().equals("")) {
			this.errorMessageUpdate("User name must not be empty");
			this.txtUserName.grabFocus();
			return false;
		}
		return true;
	}
	
	private boolean validateUserLogin () {
		if (this.txtUserLogin.getText().equals("")) {
			this.errorMessageUpdate("User login must not be empty");
			this.txtUserLogin.grabFocus();
			return false;
		}
		return true;
	}
	
	private boolean validateUserPassword () {
		if (!(this.txtUserPassword.getText().length() == 6)) {
			this.errorMessageUpdate("User password does not have the correct number of digits.");
			this.txtUserPasswordVerify.setText("");
			this.txtUserPassword.grabFocus();
			this.txtUserPassword.selectAll();
			return false;
		}
		for (int i = 1; i < this.txtUserPassword.getText().length(); i++) {
			if (this.txtUserPassword.getText().charAt(i) == this.txtUserPassword.getText().charAt(i - 1)) {
				this.errorMessageUpdate("User password cannot have equal digits together.");
				this.txtUserPasswordVerify.setText("");
				this.txtUserPassword.grabFocus();
				this.txtUserPassword.selectAll();
				return false;
			}
		}
		if (!this.stringIsNumeric(this.txtUserPassword.getText())) {
			this.errorMessageUpdate("User password must have only numbers from 0 to 9.");
			return false;
		}
		if (!this.txtUserPassword.getText().equals(this.txtUserPasswordVerify.getText())) {
			this.errorMessageUpdate("User password was not correctly verified.");
			this.txtUserPasswordVerify.grabFocus();
			this.txtUserPasswordVerify.selectAll();
			return false;
		}
		return true;
	}
	
	private boolean validateUserPublicKey () {
		if (this.txtUserPublicKeyPath.getText().equals("")) {
			this.errorMessageUpdate("User public key path must not be empty");
			this.txtUserPublicKeyPath.grabFocus();
			return false;
		}
		return true;
	}

	private boolean stringIsNumeric (String s) {
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			char c0 = "0".charAt(0);
			char c9 = "9".charAt(0);
			if (c < c0 || c > c9) {
				return false;
			}
		}
		return true;
	}
	
}
