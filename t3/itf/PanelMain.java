package itf;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import logic.Logic;

@SuppressWarnings("serial")
public class PanelMain extends JPanel implements ActionListener {

	private Dialog controller;
	private Logic logic;
	
	private JLabel user_access_count; 
	private JButton btn_register;
	private JButton btn_consult;
	private JButton btn_exit;
	
	public PanelMain (Logic logic, Dialog dialog) {
		this.logic = logic;
		this.controller = dialog;
		this.setLayout(null);
		this.setBackground(new Color(76, 184, 73));
		
		JLabel lbl_user_access = new JLabel("Number of system access:");
		lbl_user_access.setBounds(10, 10, 200, 30);
		this.add(lbl_user_access);
		this.user_access_count = new JLabel();
		this.user_access_count.setBounds(230, 10, 50, 30);
		this.add(this.user_access_count);
		
		this.btn_register = new JButton("New User");
		this.btn_register.setBounds(10, 510, 90, 30);
		this.btn_register.addActionListener(this);
		this.add(this.btn_register);
		
		this.btn_consult = new JButton("Consult");
		this.btn_consult.setBounds(110, 510, 90, 30);
		this.btn_consult.addActionListener(this);
		this.add(this.btn_consult);
		
		this.btn_exit = new JButton("Exit");
		this.btn_exit.setBounds(210, 510, 90, 30);
		this.btn_exit.addActionListener(this);
		this.add(this.btn_exit);
	}
	
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			JButton btn = (JButton) e.getSource();
			if (btn.equals(this.btn_register)) {
				this.callbackButtonRegister();
			} else if (btn.equals(this.btn_consult)) {
				this.callbackButtonConsult();
			} else if (btn.equals(this.btn_exit)) {
				this.callbackButtonExit();
			}
		}
	}
	
	public void update () {
		String group = this.logic.getGroup();
		if (group.equals("administrador")) {
			this.btn_register.setVisible(true);
		} else {
			this.btn_register.setVisible(false);
		}
		this.user_access_count.setText("" + this.logic.getAccessCount());
	}
	
	private void callbackButtonConsult () {
		this.logic.setLogMessage(5003);
		this.controller.setStateConsult();
	}
	
	private void callbackButtonRegister () {
		this.logic.setLogMessage(5002);
		this.controller.setStateRegister();
	}
	
	private void callbackButtonExit () {
		this.logic.setLogMessage(5005);
		this.controller.setStateExit();
	}
	
}
