package itf;

import javax.swing.JFrame;
import javax.swing.JPanel;

import logic.Logic;

@SuppressWarnings("serial")
public class Dialog extends JFrame {
	
	private static Integer WINDOW_WIDTH = 600;
	private static Integer WINDOW_HEIGHT = 622;
	private static Integer HEADER_HEIGHT = 50;

	private Logic logic;
	private PanelConsult panelConsult;
	private PanelExit panelExit;
	private PanelHeader panelHeader;
	private PanelLogin panelLogin;
	private PanelMain panelMain;
	private PanelRegister panelRegister;
	
	public Dialog (Logic logic) {
		this.logic = logic;
		
		this.setTitle("Encrypted Folder System");
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.setResizable(false);
		this.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		
		JPanel container = new JPanel();
		container.setLayout(null);
		this.setContentPane(container);
		
		this.panelConsult = new PanelConsult(this.logic, this);
		this.panelConsult.setBounds(0, HEADER_HEIGHT, WINDOW_WIDTH, WINDOW_HEIGHT - HEADER_HEIGHT);
		container.add(this.panelConsult);
		
		this.panelExit = new PanelExit(this.logic, this);
		this.panelExit.setBounds(0, HEADER_HEIGHT, WINDOW_WIDTH, WINDOW_HEIGHT - HEADER_HEIGHT);
		container.add(this.panelExit);
		
		this.panelHeader = new PanelHeader(this.logic);
		this.panelHeader.setBounds(0, 0, WINDOW_WIDTH, HEADER_HEIGHT);
		container.add(this.panelHeader);
		
		this.panelLogin = new PanelLogin(this.logic, this);
		this.panelLogin.setBounds(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
		container.add(this.panelLogin);

		this.panelMain = new PanelMain(this.logic, this);
		this.panelMain.setBounds(0, HEADER_HEIGHT, WINDOW_WIDTH, WINDOW_HEIGHT - HEADER_HEIGHT);
		container.add(this.panelMain);
		
		this.panelRegister = new PanelRegister(this.logic, this);
		this.panelRegister.setBounds(0, HEADER_HEIGHT, WINDOW_WIDTH, WINDOW_HEIGHT - HEADER_HEIGHT);
		container.add(this.panelRegister);

		this.setStateLogin();
	}

	public void close () {
		this.logic.close();
		super.dispose();
	}
	
	public void open () {
		this.setLocation(0, 0);
		this.setVisible(true);
	}
	
	public void setStateConsult () {
		this.logic.setLogMessage(8001);

		this.panelConsult.setVisible(true);
		this.panelExit.setVisible(false);
		this.panelHeader.setVisible(true);
		this.panelLogin.setVisible(false);
		this.panelMain.setVisible(false);
		this.panelRegister.setVisible(false);
	}
	
	public void setStateExit () {
		this.logic.setLogMessage(9001);
		
		this.panelConsult.setVisible(false);
		this.panelExit.setVisible(true);
		this.panelHeader.setVisible(true);
		this.panelLogin.setVisible(false);
		this.panelMain.setVisible(false);
		this.panelRegister.setVisible(false);
	}
	
	public void setStateLogin () {
		this.panelConsult.setVisible(false);
		this.panelExit.setVisible(false);
		this.panelHeader.setVisible(false);
		this.panelLogin.setVisible(true);
		this.panelMain.setVisible(false);
		this.panelRegister.setVisible(false);
	}

	public void setStateMain () {
		this.logic.setLogMessage(5001);
		
		this.panelConsult.setVisible(false);
		this.panelExit.setVisible(false);
		this.panelHeader.setVisible(true);
		this.panelLogin.setVisible(false);
		this.panelMain.setVisible(true);
		this.panelRegister.setVisible(false);
		this.updated();
	}
	
	public void setStateRegister () {
		this.logic.setLogMessage(6001);
		
		this.panelConsult.setVisible(false);
		this.panelExit.setVisible(false);
		this.panelHeader.setVisible(true);
		this.panelLogin.setVisible(false);
		this.panelMain.setVisible(false);
		this.panelRegister.setVisible(true);
	}
	
	public void updated () {
		this.panelConsult.update();
		this.panelExit.update();
		this.panelHeader.update();
		this.panelMain.update();
		this.panelRegister.update();
	}
	
}
