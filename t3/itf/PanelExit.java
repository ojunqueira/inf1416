package itf;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import logic.Logic;

@SuppressWarnings("serial")
public class PanelExit extends JPanel implements ActionListener {
	private Logic logic;
	private Dialog controller;

	private JLabel user_access_count; 
	private JButton btnExit;
	private JButton btnReturn;
	
	public PanelExit (Logic logic, Dialog dialog) {
		this.logic = logic;
		this.controller = dialog;

		this.setLayout(null);
		this.setBackground(new Color(247, 248, 244));
		
		JLabel lbl_user_access = new JLabel("Number of system access:");
		lbl_user_access.setBounds(10, 10, 200, 30);
		this.add(lbl_user_access);
		this.user_access_count = new JLabel();
		this.user_access_count.setBounds(230, 10, 50, 30);
		this.add(this.user_access_count);
		
		JTextArea exitMessage = new JTextArea();
		exitMessage.setBounds(100, 100, 400, 200);
		exitMessage.setLineWrap(true);
		exitMessage.setWrapStyleWord(true);
		exitMessage.setAlignmentX(CENTER_ALIGNMENT);
		exitMessage.setBackground(new Color(247, 248, 244));
		exitMessage.setEditable(false);
		exitMessage.setText("Click 'Return' button to go back to system menu or 'Exit' "
				+ "button to erase decrypted files and finish system.");
		this.add(exitMessage);
		
		this.btnExit = new JButton("Exit");
		this.btnExit.setBounds(110, 510, 90, 30);
		this.btnExit.addActionListener(this);
		this.add(this.btnExit);
		
		this.btnReturn = new JButton("Return");
		this.btnReturn.setBounds(10, 510, 90, 30);
		this.btnReturn.addActionListener(this);
		this.add(this.btnReturn);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			JButton btn = (JButton) e.getSource();
			if (btn.equals(this.btnExit)) {
				this.callbackButtonExit();
			} else if (btn.equals(this.btnReturn)) {
				this.callbackButtonReturn();
			}
		}
	}
	
	public void update () {
		this.user_access_count.setText("" + this.logic.getAccessCount());
	}

	private void callbackButtonExit () {
		this.logic.setLogMessage(9002);
		this.controller.close();
	}
	
	private void callbackButtonReturn () {
		this.logic.setLogMessage(9003);
		this.controller.setStateMain();
	}
	
}
