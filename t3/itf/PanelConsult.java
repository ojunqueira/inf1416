package itf;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import logic.Logic;

@SuppressWarnings("serial")
public class PanelConsult extends JPanel implements ActionListener, MouseListener {
	private Logic logic;
	private Dialog controller;
	
	private JLabel userConsultsNumber;
	private JTextField folderPath;
	private JLabel errorMessage;
	private JButton btnConsult;
	private JButton btnReturn;
	
	public PanelConsult (Logic logic, Dialog dialog) {
		this.logic = logic;
		this.controller = dialog;
		this.setLayout(null);
		this.setBackground(new Color(66, 134, 199));
		
		JLabel lbl_consults = new JLabel("Number of system consults:");
		lbl_consults.setBounds(10, 10, 200, 30);
		this.add(lbl_consults);
		this.userConsultsNumber = new JLabel();
		this.userConsultsNumber.setBounds(230, 10, 50, 30);
		this.add(this.userConsultsNumber);
		
		JLabel lblFolderPath = new JLabel("Open folder:");
		lblFolderPath.setBounds(50, 80, 100, 30);
		this.add(lblFolderPath);
		this.folderPath = new JTextField("/Users/otavio/Google Drive/pucSistemas/14_2_segurancaInformacao/t3/exemplo/Files");
		this.folderPath.setBounds(170, 80, 380, 30);
		this.add(this.folderPath);
		
		this.errorMessage = new JLabel();
		this.errorMessage.setBounds(50, 470, 500, 30);
		this.errorMessage.setForeground(Color.RED);
		this.add(this.errorMessage);
		this.btnConsult = new JButton("List");
		this.btnConsult.setBounds(10, 510, 90, 30);
		this.btnConsult.addActionListener(this);
		this.add(this.btnConsult);
		this.btnReturn = new JButton("Return");
		this.btnReturn.setBounds(110, 510, 90, 30);
		this.btnReturn.addActionListener(this);
		this.add(this.btnReturn);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			JButton btn = (JButton) e.getSource();
			if (btn.equals(this.btnConsult)) {
				this.callbackButtonConsult();
			} else if (btn.equals(this.btnReturn)) {
				this.callbackButtonReturn();
			}
		}
	}
	
	public void update () {
		this.userConsultsNumber.setText("" + this.logic.getConsultsCount());
	}
	
	private void callbackButtonConsult () {
		if (!this.logic.openFolder(this.folderPath.getText())) {
			this.errorMessageUpdate("Could not open folder '" + this.folderPath.getText() + "'!");
		} else {
			this.errorMessageClear();
			this.listFolderContent();
		}
		this.controller.updated();
	}
	
	private void callbackButtonReturn () {
		this.logic.setLogMessage(8002);
		
		this.controller.setStateMain();
	}
	
	private JPanel createFilePanel (String fileName, String fileSecretName, String fileStatus) {
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setName(fileName);
		panel.addMouseListener(this);
		JLabel nameLabel = new JLabel(fileName);
		nameLabel.setBounds(0, 0, 150, 30);
		panel.add(nameLabel);
		JLabel secretNameLabel = new JLabel(fileSecretName);
		secretNameLabel.setBounds(175, 0, 150, 30);
		panel.add(secretNameLabel);
		JLabel statusLabel = new JLabel(fileStatus);
		statusLabel.setBounds(350, 0, 150, 30);
		panel.add(statusLabel);
		return panel;
	}
	
	private void errorMessageUpdate (String message) {
		this.errorMessage.setText(message);
	}
	
	private void errorMessageClear () {
		this.errorMessage.setText("");
	}

	private void listFolderContent () {
		ArrayList<String> fileNames = this.logic.getFolderListFileNames();
		ArrayList<String> fileSecretNames = this.logic.getFolderListFileSecretNames();
		ArrayList<String> fileStatus = this.logic.getFolderListFileStatus();
		
		JPanel folderPanel = new JPanel();
		folderPanel.setLayout(null);
		for (int i = 0; i < fileNames.size(); i++) {
			JPanel panel = this.createFilePanel(fileNames.get(i), fileSecretNames.get(i), fileStatus.get(i));
			panel.setBounds(0, (i * 30) , 500, 30);
			folderPanel.add(panel);
		}
		folderPanel.setPreferredSize(new Dimension (500, (fileNames.size() * 30) ));
		
		JScrollPane folderScrollPanel = new JScrollPane(folderPanel);
		folderScrollPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		folderScrollPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		folderScrollPanel.setBounds(50, 150, 500, 300);
		this.add(folderScrollPanel);
		this.revalidate();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getSource() instanceof JPanel) {
			JPanel panel = (JPanel) e.getSource();
			this.logic.openFile(panel.getName());
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {		
	}

	@Override
	public void mouseExited(MouseEvent e) {		
	}
}
