package app;

import logic.Logic;
import itf.Dialog;

public class EncryptedFolderApp {

	public static void main(String[] args) {		
		Logic logic = new Logic();
		Dialog dlg = new Dialog(logic);
		dlg.open();
	}

}
