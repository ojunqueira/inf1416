package logViewer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Database {

	private static Connection con;
	private ArrayList<String> listIds;
	private ArrayList<String> listFiles;
	private ArrayList<String> listLogins;
	private ArrayList<String> listTimes;
	
	public Database () {
		
	}
	
	public boolean connect () {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://127.0.0.1/test";
			Database.con = (Connection) DriverManager.getConnection(url, "root", new String());
			return true;
		} catch (SQLException | ClassNotFoundException e) {
			System.out.println("LV Database connect(): ERROR " + e.getMessage());
			return false;
		}
	}
	
	public ArrayList<String> getListIds () {
		return this.listIds;
	}
	
	public ArrayList<String> getListFiles () {
		return this.listFiles;
	}
	
	public ArrayList<String> getListTimes () {
		return this.listTimes;
	}
	
	public ArrayList<String> getListLogins () {
		return this.listLogins;
	}
	
	public String getMessage (String messageId) {
		try {
			String sql = "SELECT * "
					+ "FROM messages "
					+ "WHERE mg_id = '" + messageId + "'"
					+ ";";
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery(sql);
			while(res.next()) {
				return res.getString("mg_message");
			}
		} catch (SQLException e) {
			System.out.println("LV Database update(): ERROR " + e.getMessage());
		}
		return null;
	}
	
	public void update (Integer lastMessageCount) {
		this.listFiles = new ArrayList<String>();
		this.listIds = new ArrayList<String>();
		this.listLogins = new ArrayList<String>();
		this.listTimes = new ArrayList<String>();
		try {
			String sql = "SELECT * "
					+ "FROM log "
					+ "WHERE lg_id > '" + lastMessageCount + "' "
					+ "ORDER BY lg_id "
					+ ";";
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery(sql);
			while(res.next()) {
				this.listFiles.add(res.getString("lg_file"));
				this.listIds.add(res.getInt("lg_message") + ""); // Can not call getNString() when field's charset isn't UTF-8
				this.listLogins.add(res.getString("lg_login"));
				this.listTimes.add(res.getTimestamp("lg_time").toString());
			}
		} catch (SQLException e) {
			System.out.println("LV Database update(): ERROR " + e.getMessage());
		}
		System.out.println("Database update: " + this.listIds.size() + " new messages.");
	}
	
}
