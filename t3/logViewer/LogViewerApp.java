package logViewer;


public class LogViewerApp {
	public static void main(String[] args) {
		Logic logic = new Logic();
		Dialog dialog = new Dialog(logic);
		dialog.open();
	}
}
