package logViewer;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.Timer;

@SuppressWarnings("serial")
public class Dialog extends JFrame implements ActionListener {

	private static Integer WINDOW_WIDTH = 600;
	private static Integer WINDOW_HEIGHT = 622;
	
	private Logic logic;
	private JList<String> list;
	private DefaultListModel<String> listModel;
	private boolean freezeUpdate;
	
	public Dialog (Logic logic) {
		this.logic = logic;
		this.freezeUpdate = false;
		
		this.setTitle("Encrypted Folder System - LOG Viewer");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		
		Timer timer = new Timer(1000, this); // 1000 to stay 1 sec
		timer.start();
		
		JPanel container = new JPanel();
		container.setLayout(null);
		this.setContentPane(container);
		
		this.listModel = new DefaultListModel<String>();
		this.list = new JList<String>(this.listModel);
		
		JScrollPane scrollPane = new JScrollPane(this.list);
		scrollPane.setBounds(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT - 80);
		container.add(scrollPane);
		this.revalidate();
		
		JButton btnFreeze = new JButton("Freeze");
		btnFreeze.setBounds(((WINDOW_WIDTH/2) - 45), WINDOW_HEIGHT - 70, 90, 30);
		btnFreeze.setBackground(new Color(88, 196, 255));
		btnFreeze.setBorderPainted(false);
		btnFreeze.addActionListener(this);
		container.add(btnFreeze);
	}
	
	public void open () {
		this.update();
		this.setLocation(610, 0);
		this.setVisible(true);
	}
	
	public void update () {
		ArrayList<String> listMessages = this.logic.getLogMessages(this.listModel.getSize());
		for (String message: listMessages) {
			this.listModel.add(0, message);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof Timer && !this.freezeUpdate) {
			this.update();
		} else if (e.getSource() instanceof JButton) {
			this.freezeUpdate = !this.freezeUpdate;
			JButton btn = (JButton) e.getSource();
			if (this.freezeUpdate) {
				btn.setOpaque(true);
			} else {
				btn.setOpaque(false);
			}
		}
	}
	
}
