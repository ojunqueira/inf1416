package logViewer;

import java.util.ArrayList;

public class Logic {

	private Database jdbc;
	
	public Logic () {
		this.jdbc = new Database();
		if (!this.jdbc.connect()) {
			System.exit(1);
		}
	}
	
	public ArrayList<String> getLogMessages (Integer lastMessageCount) {
		this.jdbc.update(lastMessageCount);
		ArrayList<String> listFiles = this.jdbc.getListFiles();
		ArrayList<String> listIds = this.jdbc.getListIds();
		ArrayList<String> listLogins = this.jdbc.getListLogins();
		ArrayList<String> listTimes = this.jdbc.getListTimes();
		ArrayList<String> listMessages = new ArrayList<String>();
		for (int i = 0; i < listFiles.size(); i++) {
			String message = this.jdbc.getMessage(listIds.get(i));
			if (!(listLogins.get(i) == null)) {
				message = message.replace("<login_name>", "'" + listLogins.get(i) + "'");
			}
			if (!(listFiles.get(i) == null)) {
				message = message.replace("<arq_name>", "'" + listFiles.get(i) + "'");
			}
			message = listIds.get(i) + "  <" + listTimes.get(i) + "> " + message;
			listMessages.add(message);
		}
		return listMessages;
	}
	
}
